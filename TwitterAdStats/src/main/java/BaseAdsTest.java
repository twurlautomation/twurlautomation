import dto.TwitterAuthenticationProfileDTO;
import dto.TwitterParametersDTO;
import org.json.simple.parser.ParseException;
import twitter4j.TwitterAds;
import twitter4j.TwitterAdsFactory;
import twitter4j.conf.ConfigurationBuilder;
import utils.Helper;

import java.io.File;
import java.io.IOException;

/**
 * Created by vinodbalasu on 18/03/17.
 */
public class BaseAdsTest {
    private static final String userDirectory = System.getProperty("user.dir");
    private static final String resourcePath = userDirectory+File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator;
    private static final String twitterProfilePath = resourcePath+"TwitterProfile.json";
    private static final String twitterParametersPath = resourcePath+"Parameters.json";

    public static TwitterAds getTwitterAdsInstance(TwitterAuthenticationProfileDTO twitterAuthenticationProfileDTO){
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setDebugEnabled(true).setOAuthAccessToken(twitterAuthenticationProfileDTO.getAccess_token()).setOAuthAccessTokenSecret(twitterAuthenticationProfileDTO.getAccess_secret()).setOAuthConsumerKey(twitterAuthenticationProfileDTO.getConsumer_key()).setOAuthConsumerSecret(twitterAuthenticationProfileDTO.getConsumer_secret()).setHttpRetryCount(0).setHttpConnectionTimeout(5000);
        return new TwitterAdsFactory(configurationBuilder.build()).getAdsInstance();
    }

    public static TwitterAuthenticationProfileDTO setTwitterAuthenticationDetails() throws IOException, ParseException {
        Object jsonObject  = Helper.loadJsonFileAndParse(twitterProfilePath);
        return (TwitterAuthenticationProfileDTO) Helper.convertJsonToDTO(jsonObject.toString(),TwitterAuthenticationProfileDTO.class);
    }


    public static TwitterParametersDTO setTwitterParameters() throws IOException, ParseException {
        Object jsonObject = Helper.loadJsonFileAndParse(twitterParametersPath);
        return (TwitterParametersDTO) Helper.convertJsonToDTO(jsonObject.toString(), TwitterParametersDTO.class);
    }
}
