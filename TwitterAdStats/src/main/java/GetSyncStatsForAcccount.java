import com.google.common.collect.Lists;
import dto.TwitterAuthenticationProfileDTO;
import dto.TwitterParametersDTO;
import org.json.simple.parser.ParseException;
import twitter4j.BaseAdsListResponse;
import twitter4j.BaseAdsListResponseIterable;
import twitter4j.TwitterAds;
import twitter4j.TwitterException;
import twitter4j.api.TwitterAdsStatApi;
import twitter4j.models.ads.TwitterEntityStatistics;
import utils.Helper;

import java.io.IOException;
import java.util.List;

/**
 * Created by vinodbalasu on 18/03/17.
 */
public class GetSyncStatsForAcccount extends BaseAdsTest {
    public static void main(String[] args) throws IOException, ParseException {

        TwitterAuthenticationProfileDTO twitterAuthenticationProfileDTO = setTwitterAuthenticationDetails();
        TwitterAds twitterAdsInstance = getTwitterAdsInstance(twitterAuthenticationProfileDTO);
        TwitterAdsStatApi statApi = twitterAdsInstance.getStatApi();
        TwitterParametersDTO twitterParametersDTO = setTwitterParameters();
        String syncStatResponse;

        List<TwitterEntityStatistics> twitterEntityStatsList = Lists.newArrayList();
        try {
            System.out.println("Fetching Sync stats");
            BaseAdsListResponseIterable<TwitterEntityStatistics> allTwitterEntityStats = statApi.fetchStatsSync(twitterParametersDTO.getAccountId(), twitterParametersDTO.getTwitterEntityType(),twitterParametersDTO.getIds(), twitterParametersDTO.getStartTime(), twitterParametersDTO.getEndTime(),twitterParametersDTO.isWithDeleted(),twitterParametersDTO.getGranularity(), twitterParametersDTO.getTwitterAdObjective(),twitterParametersDTO.getPlacement());
            for (BaseAdsListResponse<TwitterEntityStatistics> allTwitterEntityStat : allTwitterEntityStats) {
                twitterEntityStatsList.addAll(allTwitterEntityStat.getData());
            }
            //convert object to Json String and save it to file
            syncStatResponse = Helper.convertDTOToJson(twitterEntityStatsList);
            System.out.println("Sync Stat Response : \n"+syncStatResponse);
            Helper.writeStatsToFile(syncStatResponse);
        } catch (TwitterException e) {
            System.err.println(e.getErrorMessage());
        }
    }
}
