import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import dto.TwitterAuthenticationProfileDTO;
import dto.TwitterParametersDTO;
import org.json.simple.parser.ParseException;
import twitter4j.*;
import twitter4j.api.TwitterAdsStatApi;
import twitter4j.models.ads.JobDetails;
import twitter4j.models.ads.TwitterAsyncQueryStatus;
import twitter4j.models.ads.TwitterEntityStatistics;
import twitter4j.util.TwitterAdUtil;
import utils.Helper;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by vinodbalasu on 13/03/17.
 */
public class GetAsyncStatsForAccount extends BaseAdsTest {

    public static void main(String[] args) throws IOException, ParseException, TwitterException {
        TwitterAuthenticationProfileDTO twitterAuthenticationProfileDTO = setTwitterAuthenticationDetails();
        TwitterAds twitterAdsInstance = getTwitterAdsInstance(twitterAuthenticationProfileDTO);
        TwitterAdsStatApi statApi = twitterAdsInstance.getStatApi();
        TwitterParametersDTO twitterParametersDTO = setTwitterParameters();
        String asyncStatResponse = null;
        try {
            System.out.println("Creating Async Job");
            BaseAdsResponse<JobDetails> twitterAsyncJob = new BaseAdsResponse<JobDetails>();
            if (twitterParametersDTO.getTwitterSegmentationType()== null){
               twitterAsyncJob =  statApi.createAsyncJob(twitterParametersDTO.getAccountId(),twitterParametersDTO.getTwitterEntityType(),twitterParametersDTO.getIds(),twitterParametersDTO.getStartTime(),twitterParametersDTO.getEndTime(),twitterParametersDTO.isWithDeleted(),twitterParametersDTO.getGranularity(),twitterParametersDTO.getTwitterAdObjective(),twitterParametersDTO.getPlacement(),null);
            }else
            {
              twitterAsyncJob =  statApi.createAsyncJob(twitterParametersDTO.getAccountId(),twitterParametersDTO.getTwitterEntityType(),twitterParametersDTO.getIds(),twitterParametersDTO.getStartTime(),twitterParametersDTO.getEndTime(),twitterParametersDTO.isWithDeleted(),twitterParametersDTO.getGranularity(),twitterParametersDTO.getTwitterAdObjective(),twitterParametersDTO.getPlacement(),Optional.of(twitterParametersDTO.getTwitterSegmentationType()));
            }

            BaseAdsListResponseIterable<JobDetails> jobExecutionDetails;
            boolean flag;
            long timeOut = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(2);
            do {
                flag = false; //continue iterating as long as status of job of job is either queued, uploading or processing
                TwitterAdUtil.reallySleep(10000L);
                jobExecutionDetails = statApi.getJobExecutionDetails(twitterParametersDTO.getAccountId(), Lists.newArrayList(twitterAsyncJob.getData().getJobId()));
                for (BaseAdsListResponse<JobDetails> base : jobExecutionDetails) {
                    List<JobDetails> baselist = base.getData();
                    for (JobDetails jd : baselist) {
                        System.out.println("Job Status is : "+jd.getStatus());
                        if ((jd != null) && (jd.getStatus() != TwitterAsyncQueryStatus.SUCCESS)) {
                            flag = true;
                        }
                    }
                }
            } while (flag && System.currentTimeMillis() <= timeOut);

            List<TwitterEntityStatistics> twitterEntityStatsList = Lists.newArrayList();

            for (BaseAdsListResponse<JobDetails> base : jobExecutionDetails) {
                List<JobDetails> baselist = base.getData();
                for (JobDetails jd : baselist) {
                    System.out.println("Fetching Async Stats");
                    BaseAdsListResponse<TwitterEntityStatistics> allTwitterEntityStat = statApi.fetchJobDataAsync(jd.getUrl());
                    if(allTwitterEntityStat == null || allTwitterEntityStat.getData() == null){
                        continue;
                    }
                    twitterEntityStatsList.addAll(allTwitterEntityStat.getData());
                }
            }
            //convert object to Json String and save it to file
             asyncStatResponse = Helper.convertDTOToJson(twitterEntityStatsList);
             System.out.println("Async Stat Response : \n"+asyncStatResponse);
             Helper.writeStatsToFile(asyncStatResponse);
        } catch (TwitterException e) {
            System.err.println(e.getErrorMessage());
        }
    }

}
