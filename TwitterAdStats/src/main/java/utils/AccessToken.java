package utils;


import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.*;


/**
 * Created by mrunalmaniar on 21/04/17.
 */
@Path("/account")
public class AccessToken {

    @GET
    @Path("/get-token")
    @Produces(APPLICATION_JSON)
    public Response getToken(@QueryParam("id") long id) throws Exception {
        AccessTokenService accessTokenService = new AccessTokenService();
        JSONObject jsonObject = accessTokenService.getAccessToken(59837);
        accessTokenService.setTwitterProfile(jsonObject);
        String result = String.valueOf(jsonObject);

        return Response.status(200).entity(result).build();
    }

}


