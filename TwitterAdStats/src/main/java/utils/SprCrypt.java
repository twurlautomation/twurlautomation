package utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by vinodbalasu on 17/03/17.
 */
public class SprCrypt {
    private static final String SYS_PROP_SPR_ENC_KEY = "SPR_ENC_KEY";
    private static final String INTERNAL_SPR_ENC_KEY = "spr-!-nk-l-@-r";
    private static final String ENC_KEY_DIGEST = "SPRD_YsDQt6uQakdwK5cx1dxpLGM3oOqbwdUyvWxFXCDGUm1HjMEh0x+QI+GWSRNOjBgp5dih9a4WrC3ZuR3ofNp0Pg==";
    private static final String ENCRYPTED = "SPRE_";
    private static final String DIGESTED = "SPRD_";
    private static final int SALT_LENGTH = 32;
    private static final String CBC_ENCRYPTED = "SPRC_";
    private static final SecretKeySpec sprAesKey = createSprAESKey();


    /**
     * This Implementation mimics mysql aes key generation, so do not change logic for backward compatibility
     */
    private static SecretKeySpec createSprAESKey() {
        try {
            String externalEncKey = System.getProperty(SYS_PROP_SPR_ENC_KEY, "AkAsAgAmAgGjKpKpMaMsNaNpPsRmRtRkRmSmSmSkSbSsSbVsYs");
            byte[] keyBytes = (externalEncKey + "_" + INTERNAL_SPR_ENC_KEY).getBytes();
            final byte[] finalKey = new byte[32];
            int i = 0;
            for(byte b : keyBytes) {
                finalKey[i++%32] ^= b;
            }
            return new SecretKeySpec(finalKey, "AES");
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }


    public static byte[] digest(String input) {
        Random r = new SecureRandom();
        byte[] salt = new byte[SALT_LENGTH];
        r.nextBytes(salt);
        return digest(input, salt);
    }

    private static byte[] digest(String input, byte[] salt) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        messageDigest.update(salt);
        messageDigest.update(input.getBytes());
        byte[] digest = messageDigest.digest();

        byte[] saltPlusDigest = new byte[salt.length + digest.length];
        System.arraycopy(salt, 0, saltPlusDigest, 0, salt.length);
        System.arraycopy(digest, 0, saltPlusDigest, salt.length, digest.length);

        String digestBase64 = DatatypeConverter.printBase64Binary(saltPlusDigest);

        return (DIGESTED + digestBase64).getBytes();
    }

    public static String ensureEncrypted(String input) {
        try {
            SecureRandom random = new SecureRandom();
            byte iv[] = new byte[16];
            random.nextBytes(iv);
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            Cipher encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            encryptCipher.init(Cipher.ENCRYPT_MODE, sprAesKey, ivSpec);
            byte[] encryption = encryptCipher.doFinal(input.getBytes());
            byte[] ivPlusEncryption = new byte[iv.length + encryption.length];
            System.arraycopy(iv, 0, ivPlusEncryption, 0, iv.length);
            System.arraycopy(encryption, 0, ivPlusEncryption, iv.length, encryption.length);
            String encryptionBase64 = DatatypeConverter.printBase64Binary(ivPlusEncryption);
            return new String((CBC_ENCRYPTED + encryptionBase64).getBytes());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String decrypt(String input) {
        try {
            String encryptionWithoutPrefix = input.substring(ENCRYPTED.length());
            byte[] ivPlusEncryption = DatatypeConverter.parseBase64Binary(encryptionWithoutPrefix);
            byte iv[] = new byte[16];
            byte[] encryptedBytes = new byte[ivPlusEncryption.length - iv.length];
            System.arraycopy(ivPlusEncryption, 0, iv, 0, iv.length);
            System.arraycopy(ivPlusEncryption, iv.length, encryptedBytes, 0, encryptedBytes.length);
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            Cipher encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            encryptCipher.init(Cipher.DECRYPT_MODE, sprAesKey, ivSpec);
            byte[] decryptedBytes = encryptCipher.doFinal(encryptedBytes);
            return utf8String(decryptedBytes);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public static String utf8String(byte[] bytes) {
        try {
            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }


    private static byte[] encrypt(String input, SecretKeySpec keySpec, boolean withPrefix) {
        try {
            Cipher encryptCipher = Cipher.getInstance("AES");
            encryptCipher.init(Cipher.ENCRYPT_MODE, keySpec);
            byte[] encryption =  encryptCipher.doFinal(input.getBytes());
            if(withPrefix) {
                String encryptionBase64 = DatatypeConverter.printBase64Binary(encryption);
                return (ENCRYPTED + encryptionBase64).getBytes();
            } else {
                return encryption;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This Implementation mimics mysql aes key generation, so do not change logic for backward compatibility
     */
    private static SecretKeySpec createMysqlAESKey() {
        try {
            final byte[] finalKey = new byte[16];
            int i = 0;
            for(byte b : INTERNAL_SPR_ENC_KEY.getBytes()) {
                finalKey[i++%16] ^= b;
            }
            return new SecretKeySpec(finalKey, "AES");
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }




    public static void main(String[] args) {
        String digest = new String(digest("my_password"));
        //System.out.println("Digest of my_password: "+digest);

       // boolean result = matchesDigest("my_password", digest);
        // System.out.println("Matches digest: "+result);

        //result = matchesDigest("my_password1", digest);
        // System.out.println("Matches digest: "+result);

        System.out.println(decrypt("SPRC_epLjTwMWsIcFhIw2XTcMhwYHVyhdOgDOMbJ5dh7KVuFNybwxdBfudI2QfLckffYKbHFxLD6Mey5jFCaJxeeduw=="));

        // String encrypted = ensureEncrypted("SPRC_TcVgKofbsYRZ38Y56sc0mCarOOd4zNDKtiYEapcIucGgS2XN4afOhviR8wERhAhMGoSPV/AFFYgQ87ZnS4Wu/AHdQjWX/ogoYoiJfe0Jnd4hZl6qePf+KO4KvI8FwM3UNj1vSvBnMiT3OAjruned6KT3Pil5FH8ZFRo2DUglXF/6gT3tHe4LYiGxHpAwkEyZMk3UcOpaoOmxXUodUZDdzpSUlBUeOJ1VfT9LAaQSUkP0RTMRh9UDVNO15a+2w70VSzTX/ScpG89ejSxfHbh5LA==");
        // System.out.println(encrypted);
        // assert ensureDecrypted(encrypted).equals("my_password1");
    }
}
