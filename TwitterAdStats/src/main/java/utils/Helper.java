package utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.lang.reflect.Type;
/**
 * Created by vinodbalasu on 17/03/17.
 */
public class Helper {

    private static Gson gson;

    public static Object convertJsonToDTO(String json , Type type){
        gson = new Gson();
        //System.out.println("Converting Json input to " + type.getClass().getCanonicalName() + " Object ");
        Object containerObject = null;
        //System.out.println("Json is - " + json);
        try {
            containerObject = gson.fromJson(json, type);
        }
        catch (Exception ex)
        {
            System.out.println("Failed to covert JSON to " + type.getClass().getCanonicalName());
            ex.printStackTrace();
        }
        return containerObject;
    }

    public static String convertDTOToJson(Object object){
        String jsonString = null;
        //to include null fields in response
        GsonBuilder gb = new GsonBuilder();
        gb.serializeNulls();
        gson = gb.create();
        try {
           jsonString = gson.toJson(object);
        }catch (Exception ex){
            System.out.println("Failed to convert Object to Json String");
            ex.printStackTrace();
        }
        return jsonString;
    }

    public static void writeStatsToFile(String statsResponse) {
        BufferedWriter writer = null;
        String userDirectory = System.getProperty("user.dir");
        String responseFileLocation = userDirectory+File.separator+"src"+File.separator+"main"+File.separator+"response"+File.separator+"TwitterStatResponse.txt";
        try
        {
            writer = new BufferedWriter( new FileWriter(responseFileLocation));
            writer.write( statsResponse);
        }
        catch ( IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if ( writer != null)
                    writer.close( );
            }
            catch ( IOException e)
            {
                e.printStackTrace();
            }
        }
    }
    public static Object loadJsonFileAndParse(String filePath) throws IOException, ParseException {
        FileReader fileReader= new FileReader(filePath);
        JSONParser jsonParser = new JSONParser();
        return jsonParser.parse(fileReader);
    }

}
