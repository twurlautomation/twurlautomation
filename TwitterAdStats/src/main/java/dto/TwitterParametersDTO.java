package dto;

import twitter4j.models.Granularity;
import twitter4j.models.TwitterSegmentationType;
import twitter4j.models.ads.Placement;
import twitter4j.models.ads.TwitterAdObjective;
import twitter4j.models.ads.TwitterEntityType;

import java.util.Collection;

/**
 * Created by vinodbalasu on 14/03/17.
 */
public class TwitterParametersDTO {
    private String accountId;
    private TwitterEntityType twitterEntityType;
    private Collection<String> ids = null;
    private long startTime;
    private long endTime;
    private boolean withDeleted;
    private Granularity granularity;
    private TwitterAdObjective twitterAdObjective;
    private Placement placement;

    public  TwitterSegmentationType getTwitterSegmentationType() {
        return twitterSegmentationType;
    }

    public void setTwitterSegmentationType(TwitterSegmentationType twitterSegmentationType) {
        this.twitterSegmentationType = twitterSegmentationType;
    }

    public Placement getPlacement() {
        return placement;
    }

    public void setPlacement(Placement placement) {
        this.placement = placement;
    }

    public TwitterAdObjective getTwitterAdObjective() {
        return twitterAdObjective;
    }

    public void setTwitterAdObjective(TwitterAdObjective twitterAdObjective) {
        this.twitterAdObjective = twitterAdObjective;
    }

    public Granularity getGranularity() {
        return granularity;
    }

    public void setGranularity(Granularity granularity) {
        this.granularity = granularity;
    }

    public boolean isWithDeleted() {
        return withDeleted;
    }

    public void setWithDeleted(boolean withDeleted) {
        this.withDeleted = withDeleted;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public Collection<String> getIds() {
        return ids;
    }

    public void setIds(Collection<String> ids) {
        this.ids = ids;
    }

    public TwitterEntityType getTwitterEntityType() {
        return twitterEntityType;
    }

    public void setTwitterEntityType(TwitterEntityType twitterEntityType) {
        this.twitterEntityType = twitterEntityType;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    private TwitterSegmentationType twitterSegmentationType;



}
