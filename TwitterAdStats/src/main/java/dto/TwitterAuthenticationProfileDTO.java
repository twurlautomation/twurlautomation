package dto;

import utils.SprCrypt;

/**
 * Created by vinodbalasu on 17/03/17.
 */
public class TwitterAuthenticationProfileDTO {
    private String consumer_key;

    private String access_secret;

    private String consumer_secret;

    private String access_token;

    public String getConsumer_key()
    {
        return consumer_key;
    }

    public void setConsumer_key (String consumer_key)
    {
        this.consumer_key = consumer_key;
    }

    public String getAccess_secret ()
    {
        return SprCrypt.decrypt(access_secret);
    }

    public void setAccess_secret (String access_secret)
    {
        this.access_secret = access_secret;
    }

    public String getConsumer_secret ()
    {
        return SprCrypt.decrypt(consumer_secret);
    }

    public void setConsumer_secret (String consumer_secret)
    {
        this.consumer_secret = consumer_secret;
    }

    public String getAccess_token ()
    {
        return SprCrypt.decrypt(access_token);
    }

    public void setAccess_token (String access_token)
    {
        this.access_token = access_token;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [consumer_key = "+consumer_key+", access_secret = "+access_secret+", consumer_secret = "+consumer_secret+", access_token = "+access_token+"]";
    }
}
